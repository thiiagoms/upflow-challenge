# upFlow - Challenge

![](https://github.com/ekkopy/upflow-challenge/blob/master/assets/img/uplogo.png)

#### Requisitos: :hammer:
    - Você deverá criar um HTML com um campo onde será adicionado um valor numérico.
    - Ao entrar com o valor numérico e clicar no botão, a aplicação deverá replicar um volume de campos editáveis de forma dinâmica conforme o número inserido pelo usuário.
    - Ao usuário preencher os valores numéricos neste campo, deverá existir um botão de soma, que fará a soma de todos os valores preenchidos neste campos em aberto, e exibir o resultado através de uma janela.
    - Caso o usuário deixe algum campo vazio, ou preencha a informação com qualquer caractere não numérico, deverá ser tratado do erro na aplicação.
    - Apenas se todos estiverem preenchidos, a soma poderá ser feita e exibida para o usuário.

